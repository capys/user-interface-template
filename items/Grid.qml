import QtQuick 2.0

Canvas {
    id: backgroundGrid
    property int distance: 60
    anchors.fill : parent
    z:100
    onPaint: {
        var ctx = getContext("2d")
        ctx.lineWidth = 1
        ctx.strokeStyle = "lightgrey"
        ctx.beginPath()
        var nrows = height/distance;
        for(var i=0; i < nrows+1; i++){
            ctx.moveTo(0, distance*i);
            ctx.lineTo(width, distance*i);
        }
        var ncols = width/distance
        for(var j=0; j < ncols+1; j++){
            ctx.moveTo(distance*j, 0);
            ctx.lineTo(distance*j, height);
        }
        ctx.closePath()
        ctx.stroke()
    }
}
