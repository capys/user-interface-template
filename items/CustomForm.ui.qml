import QtQuick 2.4

Item {
    id: item1
    width: 100
    height: 100

    property alias buttonColor: button.color
    property alias displayText: display.text

    Rectangle {
        id: button
        color: "#e62a2a"
        radius: width * 0.5
        anchors.fill: parent

        Text {
            id: display
            x: 98
            y: 85
            color: "#fcd8d8"
            text: qsTr("Text")
            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
            font.pixelSize: 20
            font.family: "Tahoma"
        }
    }
    Image {
        width: 60
        height: 10
        anchors {
            verticalCenterOffset: 15
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
        source: "../resources/scale.png"
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:3}
}
##^##*/

