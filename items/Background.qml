import QtQuick 2.4

BackgroundForm {

    /*Property bindings*/

    property int test: 0
    property Custom target: null

    /*Keyboard input*/
    focus: true

    Keys.onPressed: {
        if (event.key == Qt.Key_1) target.width = target.width + 10
        if (event.key == Qt.Key_0) target.width = target.width - 10
    }

}
