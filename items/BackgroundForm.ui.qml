import QtQuick 2.4
import QtQuick.Window 2.12

Item {
    width: Screen.height
    height: Screen.width

    Rectangle {
        id: rectangle
        anchors.fill: parent

        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#80b1f2"
            }

            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.25}
}
##^##*/

