# user interface template

Template for user interface using QT and qml

## Description
Various methods used to perform similar functions

## Installation
Using QT 5.10 or greater will run this project, also required is QT Designeer to be able to edit items quickly
The project can be cloned and then simply run on x86 linux or if you have the b2qt toolchain installed you can run it on your embedded device.

## Usage
The project build is included. You can simply run the following command on your chose device, b2qt or x86.

## Support
BR September

## Contributing

Feel free to add any other basic operations. Ensure the code compiles. 

## Roadmap

Adding QT compiler to build artifact of the executables

## Authors and acknowledgment
BR September

## License
Copyright (c) 2022 capys

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Qt Creator is available under commercial licenses from The Qt Company, and under the GNU General Public License version 3, annotated with The Qt Company GPL Exception 1.0. See LICENSE.GPL-EXCEPT for the details.

## Project status
Currently under development
