import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import "items"
import "functions"
import "functions/componentCreator.js" as Create

/* Method 2 part 2/3: use some class in qml */
import PackageName 1.0


Window {
    width: Screen.width
    height: Screen.height
    visible: true
    title: qsTr("User Interface")

    Background {
        id: backgroundForm
        anchors.centerIn: parent
        rotation: 90
        target: sender

        /* Qml Signal
         * the custom form can be used multiple times,
         * here it is used as a sender
         */
        Sender {
            id: sender
            buttonColor: "#2a5ee6"
            displayText: "Sender"
            anchors{
                left: parent.left
                leftMargin: 10
                verticalCenter: parent.verticalCenter
            }
            target: receiver
        }

        /* Qml Slot
         * the custom form can be used multiple times,
         * here it is used as a receiver of the signal
         */
        Receiver {
            id: receiver
            displayText: "Receiver"
            width: sender.width
            anchors{
                verticalCenter: parent.verticalCenter
                right: parent.right
                rightMargin: 10
            }
        }

        /* Method 1 part 3/3: use some class in qml
         * C++ someclass calling using QmlContext
         * class gets created regardless of being used
         */
        Button {
            id: button1
            text: qsTr("Method 1 - background class: Click or Hold")
            anchors{
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: -200
                horizontalCenter: parent.horizontalCenter
            }
            onClicked: someClass.someSlot()
            onPressAndHold: someClass.someFunction()
        }

        /* Method 2 part 3/3: use some class in qml
         * C++ someclass calling using import
         * when qml page gets destroyed, so does the class
         */
        SomeClass{
            id: someClassMethod2
        }
        Button {
            id: button2
            text: qsTr("Method 2 - Created in qml: Click or Hold")
            anchors{
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: 150
                horizontalCenter: parent.horizontalCenter
            }
            onClicked: someClassMethod2.someSlot()
            onPressAndHold: someClassMethod2.someFunction()
        }

        /* Demonstrates how variables can be passed to qml
         * Either Method 1 or Method 2 can be used
         * text: someClass.someVar or text: someClassMethod2.someVar
         */
        Text {
            id: text1
            anchors{
                top: parent.top
                topMargin: 0
                horizontalCenter: parent.horizontalCenter
            }
            /* someClass is not yet defined, so can't be used here */
            text: someClassMethod2.someVar
            font.pixelSize: 30
        }

        /* Alternative getting variable Part 2/2
         * A signal needs to be connected
         */
        Connections {
            target: someClassMethod2
            onSomeVarChanged: text2.text = someClassMethod2.getsomeVar()
        }
        Text {
            id: text2
            anchors{
                top: parent.top
                topMargin: 40
                horizontalCenter: parent.horizontalCenter
            }
            text: someClassMethod2.getsomeVar()
            font.pixelSize: 30
        }

        Button {
            id: button3
//            width: 56
//            height: 40
            text: qsTr("Click/Hold : Enum as property and slot vs q_invoke")
            anchors{
                verticalCenter: parent.verticalCenter
                horizontalCenterOffset: 0
                verticalCenterOffset: -87
                horizontalCenter: parent.horizontalCenter
            }
            onPressAndHold: someClassMethod2.setsomeVar("Click Again")
            onClicked:{
                someClassMethod2.setsomeVar("Key Press 1 or 0")
                someClassMethod2.setstatus( 2 )
                console.log(someClassMethod2.status)

                /* Not sure why the following does not work */
                //console.log(someClassMethod2.Ready)
            }
        }

        /* using .js file to create a component or by simply using Grid, the script is imported
         * when the button is clicked it will create a new Grid using the js file
         */
        Rectangle {
            id: place_holder
            width: 200
            height: 200
            color: "#ffffff"
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            Grid{
                id: mygrid
                distance: 30
                anchors.fill: place_holder
            }
            Button{
                text: "Small Grid"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: Create.newGrid(10)
            }
        }

    }/* End of Background */
}/* End of Window */

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.5;height:480;width:640}
}
##^##*/
