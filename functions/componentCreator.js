var grid = null

/* always use small case for first letter of functions */

function newGrid(blocksize){
    if(grid !== null)
        grid.destroy()

    var component = Qt.createComponent("../items/Grid.qml");
    grid= component.createObject(place_holder,{"anchors.fill": place_holder, "distance": blocksize});

}
