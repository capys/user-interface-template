import QtQuick 2.0
import "../items"

Custom {

    id: receiveButton

    function receive(value){
        displayText = value
        clicknotify.running = true
    }

    SequentialAnimation on buttonColor {
        id: clicknotify
        running: false

        ColorAnimation {
            from: "red"
            to: "blue"
            duration: 1000
        }

    }

}
