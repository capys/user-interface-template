#include <QGuiApplication>
#include <QQmlApplicationEngine>

/* Used for Method 1 */
#include <QQmlContext>

/* Used for both Methods */
#include "someclass.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    /* Method 1 part 1/3: use some class in qml */
    SomeClass someClass;

    /* Method 2 part 1/3: use some class in qml via import */
    qmlRegisterType < SomeClass > ( "PackageName",1,0,"SomeClass");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);


    /* Method 1 part 2/3: use some class in qml */
    QQmlContext * rootContext = engine.rootContext();
    rootContext->setContextProperty( "someClass", &someClass );

    return app.exec();
}
