#include "someclass.h"
#include "QDebug"

SomeClass::SomeClass(QObject *parent) : QObject(parent)
{
    qDebug() << "Some class created...";
}

void SomeClass::someFunction(void)
{
    qDebug() << "Some function running...";
}

void SomeClass::someSlot(void)
{
    qDebug() <<"Some slot being called!";
}




QString SomeClass::someVar(void)
{
    return m_someVar;
}

SomeClass::Status SomeClass::status(void)
{
    return m_status;
}

QString SomeClass::getsomeVar()
{
    return m_someVar;
}

void SomeClass::setsomeVar(QString newVar)
{
    if (m_someVar != newVar){
        m_someVar = newVar;
        emit someVarChanged();
    }
    else{

    }
}

void SomeClass::setstatus(Status newStatus)
{
    if (m_status != newStatus){
        m_status = newStatus;
        emit statusChanged();
    }
    else{
    }
    qDebug() <<newStatus;
}
