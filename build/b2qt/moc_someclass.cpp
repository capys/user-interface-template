/****************************************************************************
** Meta object code from reading C++ file 'someclass.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../someclass.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'someclass.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SomeClass_t {
    QByteArrayData data[18];
    char stringdata0[158];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SomeClass_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SomeClass_t qt_meta_stringdata_SomeClass = {
    {
QT_MOC_LITERAL(0, 0, 9), // "SomeClass"
QT_MOC_LITERAL(1, 10, 14), // "someVarChanged"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 13), // "statusChanged"
QT_MOC_LITERAL(4, 40, 8), // "someSlot"
QT_MOC_LITERAL(5, 49, 10), // "setsomeVar"
QT_MOC_LITERAL(6, 60, 6), // "newVar"
QT_MOC_LITERAL(7, 67, 9), // "setstatus"
QT_MOC_LITERAL(8, 77, 6), // "Status"
QT_MOC_LITERAL(9, 84, 9), // "newStatus"
QT_MOC_LITERAL(10, 94, 12), // "someFunction"
QT_MOC_LITERAL(11, 107, 10), // "getsomeVar"
QT_MOC_LITERAL(12, 118, 7), // "someVar"
QT_MOC_LITERAL(13, 126, 6), // "status"
QT_MOC_LITERAL(14, 133, 4), // "Null"
QT_MOC_LITERAL(15, 138, 5), // "Ready"
QT_MOC_LITERAL(16, 144, 7), // "Loading"
QT_MOC_LITERAL(17, 152, 5) // "Error"

    },
    "SomeClass\0someVarChanged\0\0statusChanged\0"
    "someSlot\0setsomeVar\0newVar\0setstatus\0"
    "Status\0newStatus\0someFunction\0getsomeVar\0"
    "someVar\0status\0Null\0Ready\0Loading\0"
    "Error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SomeClass[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       2,   60, // properties
       1,   68, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   51,    2, 0x0a /* Public */,
       5,    1,   52,    2, 0x0a /* Public */,
       7,    1,   55,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      10,    0,   58,    2, 0x02 /* Public */,
      11,    0,   59,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, 0x80000000 | 8,    9,

 // methods: parameters
    QMetaType::Void,
    QMetaType::QString,

 // properties: name, type, flags
      12, QMetaType::QString, 0x00495003,
      13, 0x80000000 | 8, 0x0049500b,

 // properties: notify_signal_id
       0,
       1,

 // enums: name, flags, count, data
       8, 0x0,    4,   72,

 // enum data: key, value
      14, uint(SomeClass::Null),
      15, uint(SomeClass::Ready),
      16, uint(SomeClass::Loading),
      17, uint(SomeClass::Error),

       0        // eod
};

void SomeClass::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SomeClass *_t = static_cast<SomeClass *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->someVarChanged(); break;
        case 1: _t->statusChanged(); break;
        case 2: _t->someSlot(); break;
        case 3: _t->setsomeVar((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->setstatus((*reinterpret_cast< Status(*)>(_a[1]))); break;
        case 5: _t->someFunction(); break;
        case 6: { QString _r = _t->getsomeVar();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (SomeClass::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SomeClass::someVarChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SomeClass::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SomeClass::statusChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        SomeClass *_t = static_cast<SomeClass *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->someVar(); break;
        case 1: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        SomeClass *_t = static_cast<SomeClass *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setsomeVar(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setstatus(*reinterpret_cast< Status*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject SomeClass::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SomeClass.data,
      qt_meta_data_SomeClass,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SomeClass::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SomeClass::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SomeClass.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int SomeClass::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void SomeClass::someVarChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void SomeClass::statusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
