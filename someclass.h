#ifndef SOMECLASS_H
#define SOMECLASS_H

#include <QObject>

class SomeClass : public QObject
{
    Q_OBJECT
public:
    explicit SomeClass(QObject *parent = nullptr);

    /* Calling a function from qml"  */
    Q_INVOKABLE void someFunction(void);

    /* Operation of some variable */
    Q_PROPERTY(QString someVar READ someVar WRITE setsomeVar NOTIFY someVarChanged)    

    enum Status{
        Null,
        Ready,
        Loading,
        Error
    };
    Q_ENUM(Status)
    Q_PROPERTY(Status status READ status WRITE setstatus NOTIFY statusChanged)

    /* Reading some variable */
    QString someVar(void);
    Status status(void);

    /* Alternative getting variable Part 1/2 */
    Q_INVOKABLE QString getsomeVar(void);

signals:
    /* Checking the change of some variable
     * Ensure signals start with lower case
     */
    void someVarChanged(void);
    void statusChanged(void);

public slots:

    /* Can use Q_INVOKABLE alternative */
    void someSlot(void);

    /* Setting some variable */
    void setsomeVar (QString newVar);
    void setstatus (Status newStatus);

private:
    /* Initializing some variable */
    QString m_someVar = "Key Press 1 or 0";
    Status m_status = Loading;
};

#endif // SOMECLASS_H
